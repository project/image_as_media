## Image as Media

Provides a field widget and field formatter for image fields to look up a corresponding media item based on the image file.

This is meant for sites that were migrated from Drupal 7 media module in an incomplete state, and will allow you to leverage migrated media entities using the migrated image fields for new content until you are ready to change those image fields into media reference fields.

### The media migration problem

There is no "perfect" media migration from Drupal 7 that migrates image or file fields into media reference fields.

A module like [Media Migration](https://drupal.org/project/media_migration) may not have migrated image or file fields into media reference fields putting your site into an incomplete migrated state where image fields and media are _not-related at all_.

And now you can't add content because any new images will not have a corresponding media item created!

What now?

### The stop-gap solution

This module provides a stop-gap solution so that you _can_ add content without screwing up your site's content.

1. An image field formatter "Media entity" so that you can access fields on the media entity in views and such.
2. An image field widget "Media Select" so that you can add a media (image) entity and save the file related to that on your image field.

**This should not be considered a permanent solution!**

You will eventually need to duplicate your image fields as media entity reference fields using one of the modules below in the "Follow-up solutions" section.

#### Module requirements

- [Inline Entity Form](https://drupal.org/project/inline_entity_form)

### Follow-up solutions

Ideally you do not want to continue using this module forever. You can use one of the following modules to duplicate image fields into media entity reference fields so that you can redo views, layouts, theming, etc...  when you are ready to do so on your site. Then you can hide (or delete) the old fields.

- [Migrate File Entities to Media Entities](https://drupal.org/project/migrate_file_to_media)
- [Image to Media migrate](https://drupal.org/project/image_media_migrate)
- [Image field to media](https://drupal.org/project/image_field_to_media)
- [File To Media](https://drupal.org/project/file_to_media)
- [File-Field to Media-Field](https://drupal.org/project/filefield_to_mediafield)

### Similar modules

- [Image Library Widget](https://drupal.org/project/image_library_widget)
   - Does not provide a formatter so any fields that are attached to the media entity are not available.
