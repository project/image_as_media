<?php

namespace Drupal\image_as_media;

use Drupal\file\FileInterface;

/**
 * Provides common method for getting media entity from file entity.
 */
trait MediaFileUsageTrait {

  protected function getDatabase() {
    return $this->database ?? \Drupal::database();
  }

  protected function getEntityTypeManager() {
    return $this->entityTypeManager ?? \Drupal::entityTypeManager();
  }

  /**
   * Get the media entity for the corresponding file entity based on usage.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   *
   * @return \Drupal\media\MediaInterface|void
   *   The media entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMediaForFile(FileInterface $file) {
    $media_id = $this->getDatabase()
      ->select('file_usage')
      ->fields('file_usage', ['id'])
      ->condition('fid', $file->id())
      ->condition('type', 'media')
      ->execute()
      ->fetchField();

    if ($media_id) {
      /** @var \Drupal\media\MediaInterface $media */
      $media = $this->getEntityTypeManager()
        ->getStorage('media')
        ->load($media_id);
      if ($media) {
        return $media;
      }
    }
  }

  /**
   * Get the media entity for the corresponding file entity based on usage.
   *
   * @param int $fid
   *   The file entity id.
   * @param string $target_bundle
   *   The target_bundle to use if creating a new media entity.
   *
   * @return \Drupal\media\MediaInterface|void
   *   The media entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMediaForFileId($fid, $target_bundle = NULL) {
    if (!$fid) {
      return NULL;
    }

    $media_id = $this->getDatabase()
       ->select('file_usage')
       ->fields('file_usage', ['id'])
       ->condition('fid', $fid)
       ->condition('type', 'media')
       ->execute()
       ->fetchField();

    /** @var \Drupal\media\MediaInterface $media */
    if ($media_id) {
      $media = $this->getEntityTypeManager()
        ->getStorage('media')
        ->load($media_id);
      if ($media) {
        return $media;
      }
    }

    // Create a new media entity because somehow the old one was removed, and
    // there is still a lingering file reference.
    $media = $this->getEntityTypeManager()
      ->getStorage('media')
      ->create(['bundle' => $target_bundle]);
    $media->field_media_image = $fid;
    return $media;
  }

}
