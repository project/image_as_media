<?php

namespace Drupal\image_as_media\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image_as_media\MediaFileUsageTrait;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;
use Drupal\inline_entity_form\TranslationHelper;
use Drupal\media\MediaTypeInterface;

/**
 * Defines the image media select widget that extends entity_inline_form.
 *
 * This allows to add a media image while adding a node. Save operations MUST be
 * separated from the node save because of field widgets are not capable of
 * hooking into node save operations other than providing a values for a field
 * type and its field storage.
 *
 * There is a lot of code duplication between the parent class because certain
 * variables within the parent method need to be changed to support a file to
 * media lookup and vice versa.
 *
 * @FieldWidget(
 *   id = "image_as_media_widget",
 *   label = @Translation("Media"),
 *   description = @Translation("Allows users to upload images as media."),
 *   field_types = {
 *     "image"
 *   },
 *   multiple_values = true
 * )
 */
class ImageAsMediaWidget extends InlineEntityFormComplex {

  use MediaFileUsageTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'target_bundle' => 'image',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['target_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Target bundle'),
      '#description' => $this->t('Select the target media bundle associated with this image field.'),
      '#default_value' => $this->getSetting('target_bundle'),
      '#options' => $this->getMediaBundles(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Media bundle: @placeholder', ['@placeholder' => $this->getSetting('target_bundle')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $target_bundle = $this->getSetting('target_bundle');
    // Get the entity type labels for the UI strings.
    $labels = $this->getEntityTypeLabels();

    // Build a parents array for this element's values in the form.
    $parents = array_merge($element['#field_parents'], [
      $items->getName(),
      'form',
    ]);

    // Assign a unique identifier to each IEF widget.
    // Since $parents can get quite long, hashing ensures that every id has
    // a consistent and relatively short length while maintaining uniqueness.
    $this->setIefId($this->makeIefId($parents));

    // Get the langcode of the parent entity.
    $parent_langcode = $items->getEntity()->language()->getId();

    // Determine the wrapper ID for the entire element.
    $wrapper = 'inline-entity-form-' . $this->getIefId();

    $element += [
      '#type' => $this->getSetting('collapsible') ? 'details' : 'fieldset',
      '#tree' => TRUE,
      '#description' => $this->fieldDefinition->getDescription(),
      '#prefix' => '<div id="' . $wrapper . '">',
      '#suffix' => '</div>',
      '#ief_id' => $this->getIefId(),
      '#ief_root' => TRUE,
      '#translating' => $this->isTranslating($form_state),
      '#field_title' => $this->fieldDefinition->getLabel(),
      '#after_build' => [
        [get_class($this), 'removeTranslatabilityClue'],
      ],
    ];
    if ($element['#type'] === 'details') {
      // If there's user input, keep the details open. Otherwise, use settings.
      $element['#open'] = $form_state->getUserInput() ?: !$this->getSetting('collapsed');
    }

    $this->prepareFormState($form_state, $items, $element['#translating']);
    $entities = $form_state->get(['inline_entity_form', $this->getIefId(), 'entities']);

    // Prepare cardinality information.
    $entities_count = count($entities);
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $cardinality_reached = ($cardinality > 0 && $entities_count == $cardinality);

    // Build the "Multiple value" widget.
    // TODO - does this belong in #element_validate?
    $element['#element_validate'][] = [get_class($this), 'updateRowWeights'];
    // Add the required element marker & validation.
    if ($element['#required']) {
      $element['#element_validate'][] = [get_class($this), 'requiredField'];
    }

    $element['entities'] = [
      '#tree' => TRUE,
      '#theme' => 'inline_entity_form_entity_table',
      '#entity_type' => 'media',
    ];

    // Get the fields that should be displayed in the table.
    $fields = $this->inlineFormHandler->getTableFields([$target_bundle]);
    $context = [
      'parent_entity_type' => $this->fieldDefinition->getTargetEntityTypeId(),
      'parent_bundle' => $this->fieldDefinition->getTargetBundle(),
      'field_name' => $this->fieldDefinition->getName(),
      'entity_type' => 'media',
      'allowed_bundles' => [$target_bundle],
    ];
    $this->moduleHandler->alter('inline_entity_form_table_fields', $fields, $context);
    $element['entities']['#table_fields'] = $fields;

    $weight_delta = max(ceil($entities_count * 1.2), 50);
    foreach ($entities as $key => $value) {
      // Data used by inline-entity-form-entity-table.html.twig.
      // @see template_preprocess_inline_entity_form_entity_table()
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $value['entity'];
      $element['entities'][$key]['#label'] = $this->inlineFormHandler->getEntityLabel($value['entity']);
      $element['entities'][$key]['#entity'] = $value['entity'];
      $element['entities'][$key]['#needs_save'] = $value['needs_save'];

      // Handle row weights.
      $element['entities'][$key]['#weight'] = $value['weight'];

      // First check to see if this entity should be displayed as a form.
      if (!empty($value['form'])) {
        $element['entities'][$key]['title'] = [];
        $element['entities'][$key]['delta'] = [
          '#type' => 'value',
          '#value' => $value['weight'],
        ];

        // Add the appropriate form.
        if (in_array($value['form'], ['edit', 'duplicate'])) {
          $element['entities'][$key]['form'] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['ief-form', 'ief-form-row']],
            'inline_entity_form' => $this->getInlineEntityForm(
              $value['form'],
              $entity->bundle(),
              $parent_langcode,
              $key,
              array_merge($parents, ['inline_entity_form', 'entities', $key, 'form']),
              $value['form'] == 'edit' ? $entity : $entity->createDuplicate()
            ),
          ];

          $element['entities'][$key]['form']['inline_entity_form']['#process'] = [
            ['\Drupal\inline_entity_form\Element\InlineEntityForm', 'processEntityForm'],
            [get_class($this), 'addIefSubmitCallbacks'],
            [get_class($this), 'buildEntityFormActions'],
          ];
        }
        elseif ($value['form'] == 'remove') {
          $element['entities'][$key]['form'] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['ief-form', 'ief-form-row']],
            // Used by Field API and controller methods to find the relevant
            // values in $form_state.
            '#parents' => array_merge($parents, ['entities', $key, 'form']),
            // Store the entity on the form, later modified in the controller.
            '#entity' => $entity,
            // Identifies the IEF widget to which the form belongs.
            '#ief_id' => $this->getIefId(),
            // Identifies the table row to which the form belongs.
            '#ief_row_delta' => $key,
          ];
          $this->buildRemoveForm($element['entities'][$key]['form']);
        }
      }
      else {
        $row = &$element['entities'][$key];
        $row['title'] = [];
        $row['delta'] = [
          '#type' => 'weight',
          '#delta' => $weight_delta,
          '#default_value' => $value['weight'],
          '#attributes' => ['class' => ['ief-entity-delta']],
        ];
        // Add an actions container with edit and delete buttons for the entity.
        $row['actions'] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['ief-entity-operations']],
        ];

        // Make sure entity_access is not checked for unsaved entities.
        $entity_id = $entity->id();
        if (empty($entity_id) || $entity->access('update')) {
          $row['actions']['ief_entity_edit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Edit'),
            '#name' => 'ief-' . $this->getIefId() . '-entity-edit-' . $key,
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => 'inline_entity_form_get_element',
              'wrapper' => $wrapper,
            ],
            '#submit' => ['inline_entity_form_open_row_form'],
            '#ief_row_delta' => $key,
            '#ief_row_form' => 'edit',
          ];
        }

        // Add the duplicate button, if allowed.
        if ($settings['allow_duplicate'] && !$cardinality_reached && $entity->access('create')) {
          $row['actions']['ief_entity_duplicate'] = [
            '#type' => 'submit',
            '#value' => $this->t('Duplicate'),
            '#name' => 'ief-' . $this->getIefId() . '-entity-duplicate-' . $key,
            '#limit_validation_errors' => [array_merge($parents, ['actions'])],
            '#ajax' => [
              'callback' => 'inline_entity_form_get_element',
              'wrapper' => $wrapper,
            ],
            '#submit' => ['inline_entity_form_open_row_form'],
            '#ief_row_delta' => $key,
            '#ief_row_form' => 'duplicate',
          ];
        }

        // If 'allow_existing' is on, the default removal operation is unlink
        // and the access check for deleting happens inside the controller
        // removeForm() method.
        if (empty($entity_id) || $settings['allow_existing'] || $entity->access('delete')) {
          $row['actions']['ief_entity_remove'] = [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
            '#name' => 'ief-' . $this->getIefId() . '-entity-remove-' . $key,
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => 'inline_entity_form_get_element',
              'wrapper' => $wrapper,
            ],
            '#submit' => ['inline_entity_form_open_row_form'],
            '#ief_row_delta' => $key,
            '#ief_row_form' => 'remove',
            '#access' => !$element['#translating'],
          ];
        }
      }
    }

    // When in translation, the widget only supports editing (translating)
    // already added entities, so there's no need to show the rest.
    if ($element['#translating']) {
      if (empty($entities)) {
        // There are no entities available for translation, hide the widget.
        $element['#access'] = FALSE;
      }
      return $element;
    }

    if ($cardinality > 1) {
      // Add a visual cue of cardinality count.
      $message = $this->t('You have added @entities_count out of @cardinality_count allowed @label.', [
        '@entities_count' => $entities_count,
        '@cardinality_count' => $cardinality,
        '@label' => $labels['plural'],
      ]);
      $element['cardinality_count'] = [
        '#markup' => '<div class="ief-cardinality-count">' . $message . '</div>',
      ];
    }
    // Do not return the rest of the form if cardinality count has been reached.
    if ($cardinality_reached) {
      return $element;
    }

    $allow_new = $settings['allow_new'];
    $hide_cancel = FALSE;
    // If the field is required and empty try to open one of the forms.
    if (empty($entities) && $this->fieldDefinition->isRequired()) {
      if ($settings['allow_existing'] && !$allow_new) {
        $form_state->set(['inline_entity_form', $this->getIefId(), 'form'], 'ief_add_existing');
        $hide_cancel = TRUE;
      }
      elseif ($allow_new && !$settings['allow_existing']) {
        $bundle = $target_bundle;

        // The parent entity type and bundle must not be the same as the inline
        // entity type and bundle, to prevent recursion.
        $parent_entity_type = $this->fieldDefinition->getTargetEntityTypeId();
        $parent_bundle = $this->fieldDefinition->getTargetBundle();
        if ($parent_entity_type !== 'media' || $parent_bundle !== $bundle) {
          $form_state->set(['inline_entity_form', $this->getIefId(), 'form'], 'add');
          $form_state->set(['inline_entity_form', $this->getIefId(), 'form settings'], [
            'bundle' => $bundle,
          ]);
          $hide_cancel = TRUE;
        }
      }
    }

    // If no form is open, show buttons that open one.
    $open_form = $form_state->get(['inline_entity_form', $this->getIefId(), 'form']);

    if (empty($open_form)) {
      $element['actions'] = [
        '#attributes' => ['class' => ['container-inline']],
        '#type' => 'container',
        '#weight' => 100,
      ];

      // The user is allowed to create an entity of at least one bundle.
      if ($allow_new) {
        $element['actions']['bundle'] = [
          '#type' => 'value',
          '#value' => $target_bundle,
        ];

        $element['actions']['ief_add'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add new @type_singular', ['@type_singular' => $labels['singular']]),
          '#name' => 'ief-' . $this->getIefId() . '-add',
          '#limit_validation_errors' => [array_merge($parents, ['actions'])],
          '#ajax' => [
            'callback' => 'inline_entity_form_get_element',
            'wrapper' => $wrapper,
          ],
          '#submit' => ['inline_entity_form_open_form'],
          '#ief_form' => 'add',
        ];
      }

      if ($settings['allow_existing']) {
        $element['actions']['ief_add_existing'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add existing @type_singular', ['@type_singular' => $labels['singular']]),
          '#name' => 'ief-' . $this->getIefId() . '-add-existing',
          '#limit_validation_errors' => [array_merge($parents, ['actions'])],
          '#ajax' => [
            'callback' => 'inline_entity_form_get_element',
            'wrapper' => $wrapper,
          ],
          '#submit' => ['inline_entity_form_open_form'],
          '#ief_form' => 'ief_add_existing',
        ];
      }
    }
    else {
      // Make a delta key bigger than all existing ones, without assuming that
      // the keys are strictly consecutive.
      $new_key = $entities ? max(array_keys($entities)) + 1 : 0;
      // There's a form open, show it.
      if ($form_state->get(['inline_entity_form', $this->getIefId(), 'form']) == 'add') {
        $element['form'] = [
          '#type' => 'fieldset',
          '#attributes' => ['class' => ['ief-form', 'ief-form-bottom']],
          'inline_entity_form' => $this->getInlineEntityForm(
            'add',
            $target_bundle,
            $parent_langcode,
            $new_key,
            array_merge($parents, [$new_key])
          ),
        ];
        $element['form']['inline_entity_form']['#process'] = [
          ['\Drupal\inline_entity_form\Element\InlineEntityForm', 'processEntityForm'],
          [get_class($this), 'addIefSubmitCallbacks'],
          [get_class($this), 'buildEntityFormActions'],
        ];
      }
      elseif ($form_state->get(['inline_entity_form', $this->getIefId(), 'form']) == 'ief_add_existing') {
        $element['form'] = [
          '#type' => 'fieldset',
          '#attributes' => ['class' => ['ief-form', 'ief-form-bottom']],
          // Identifies the IEF widget to which the form belongs.
          '#ief_id' => $this->getIefId(),
          // Used by Field API and controller methods to find the relevant
          // values in $form_state.
          '#parents' => array_merge($parents, [$new_key]),
          '#entity_type' => 'media',
          '#ief_labels' => $this->getEntityTypeLabels(),
          '#match_operator' => $this->getSetting('match_operator'),
        ];

        $element['form'] += $this->getMediaReferenceForm($element['form'], $form_state);
      }

      // Pre-opened forms can't be closed in order to force the user to
      // add / reference an entity.
      if ($hide_cancel) {
        if ($open_form == 'add') {
          $process_element = &$element['form']['inline_entity_form'];
        }
        elseif ($open_form == 'ief_add_existing') {
          $process_element = &$element['form'];
        }
        $process_element['#process'][] = [get_class($this), 'hideCancel'];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    if ($this->isDefaultValueWidget($form_state)) {
      $items->filterEmptyItems();
      return;
    }
    $triggering_element = $form_state->getTriggeringElement();
    if (empty($triggering_element['#ief_submit_trigger'])) {
      return;
    }

    $field_name = $this->fieldDefinition->getName();
    $target_bundles = [
      $this->getSetting('target_bundle') => $this->getSetting('target_bundle'),
    ];
    $parents = array_merge($form['#parents'], [$field_name, 'form']);
    $ief_id = $this->makeIefId($parents);
    $this->setIefId($ief_id);
    $widget_state = &$form_state->get(['inline_entity_form', $ief_id]);
    foreach ($widget_state['entities'] as $key => $value) {
      $changed = TranslationHelper::updateEntityLangcode($value['entity'], $form_state);
      if ($changed) {
        $widget_state['entities'][$key]['entity'] = $value['entity'];
        $widget_state['entities'][$key]['needs_save'] = TRUE;
      }
    }

    // Maps existing entities into image reference values.
    $values = array_map(function (array $value) {
      return [
        'target_id' => $value['entity']->field_media_image->target_id,
        'weight' => $value['weight'],
      ];
    }, $widget_state['entities']);

    // If the inline entity form is still open, then its entity hasn't
    // been transferred to the IEF form state yet.
    if (empty($values) && !empty($widget_state['form'])) {
      if ($widget_state['form'] == 'add') {
        $element = NestedArray::getValue($form, [$field_name, 'widget', 'form']);
        $entity = $element['inline_entity_form']['#entity'];
        $values[] = ['target_id' => $entity->field_media_image->target_id];
      }
      elseif ($widget_state['form'] == 'ief_add_existing') {
        $parent = NestedArray::getValue($form, [$field_name, 'widget', 'form']);
        $element = isset($parent['entity_id']) ? $parent['entity_id'] : [];
        if (!empty($element['#value'])) {
          $options = [
            'target_type' => 'media',
            'handler' => 'default:media',
            'target_bundles' => $target_bundles,
            'sort' => ['field' => '_none', 'direction' => 'ASC'],
            'auto_create' => FALSE,
            'auto_create_bundle' => '',
          ];
          /** @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler */
          $handler = $this->selectionManager->getInstance($options);
          $input_values = $element['#tags'] ? Tags::explode($element['#value']) : [$element['#value']];

          foreach ($input_values as $input) {
            $match = EntityAutocomplete::extractEntityIdFromAutocompleteInput($input);
            if ($match === NULL) {
              // Try to get a match from the input string when the user didn't use
              // the autocomplete but filled in a value manually.
              $entities_by_bundle = $handler->getReferenceableEntities($input, '=');
              $entities = array_reduce($entities_by_bundle, function ($flattened, $bundle_entities) {
                return $flattened + $bundle_entities;
              }, []);
              $params = [
                '%value' => $input,
                '@value' => $input,
              ];
              if (empty($entities)) {
                $form_state->setError($element, $this->t('There are no entities matching "%value".', $params));
              }
              elseif (count($entities) > 5) {
                $params['@id'] = key($entities);
                // Error if there are more than 5 matching entities.
                $form_state->setError($element, $this->t('Many entities are called %value. Specify the one you want by appending the id in parentheses, like "@value (@id)".', $params));
              }
              elseif (count($entities) > 1) {
                // More helpful error if there are only a few matching entities.
                $multiples = [];
                foreach ($entities as $id => $name) {
                  $multiples[] = $name . ' (' . $id . ')';
                }
                $params['@id'] = $id;
                $form_state->setError($element, $this->t('Multiple entities match this reference; "%multiple". Specify the one you want by appending the id in parentheses, like "@value (@id)".', ['%multiple' => implode('", "', $multiples)] + $params));
              }
              else {
                // Take the one and only matching entity.
                $entity = reset($entities);
                $values += [
                  'target_id' => $entity->field_media_image->target_id,
                ];
              }
            }
            else {
              $media = $this->entityTypeManager->getStorage('media')->load($match);
              $values += [
                'target_id' => $media->field_media_image->target_id,
              ];
            }
          }
        }
      }
    }
    // Sort values by weight.
    uasort($values, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    // Let the widget massage the submitted values.
    $values = $this->massageFormValues($values, $form, $form_state);
    // Assign the values and remove the empty ones.
    $items->setValue($values);
    $items->filterEmptyItems();
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareFormState(FormStateInterface $form_state, FieldItemListInterface $items, $translating = FALSE) {
    $widget_state = $form_state->get(['inline_entity_form', $this->iefId]);
    if (empty($widget_state)) {
      $widget_state = [
        'instance' => $this->fieldDefinition,
        'form' => NULL,
        'delete' => [],
        'entities' => [],
      ];

      // Store the $items entities in the widget state, for further manipulation.
      /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $item */
      foreach ($items as $delta => $item) {
        $entity = $this->getMediaForFileId($item->target_id, $this->getSetting('target_bundle'));
        if ($translating && $entity) {
          $entity = TranslationHelper::prepareEntity($entity, $form_state);
        }
        $widget_state['entities'][$delta] = [
          'entity' => $entity,
          'weight' => $delta,
          'form' => NULL,
          'needs_save' => $entity->isNew(),
        ];
      }
      $form_state->set(['inline_entity_form', $this->iefId], $widget_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getInlineEntityForm($operation, $bundle, $langcode, $delta, array $parents, EntityInterface $entity = NULL) {
    $element = parent::getInlineEntityForm($operation, $bundle, $langcode, $delta, $parents, $entity);

    // The media entity must be saved because this is a widget.
    $element['#entity_type'] = 'media';
    $element['#save_entity'] = TRUE;

    return $element;
  }

  /**
   * Get the available media bundles.
   *
   * @return string[]
   *   An array of options for a select list.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getMediaBundles() {
    $options = [];
    $media_types = $this->entityTypeManager
      ->getStorage('media_type')
      ->loadMultiple();
    $image_types = array_filter($media_types, function (MediaTypeInterface $entity) {
      return $entity->getSource()->getPluginId() === 'image';
    });
    foreach ($image_types as $image_type) {
      $options[$image_type->id()] = Xss::filter($image_type->label());
    }
    return $options;
  }

  /**
   * Gets an entity reference form for media with a default handler.
   *
   * @todo provide a means of changing the selection settings.
   *
   * @param array $reference_form
   *   The form element from the caller.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The reference form to attach to the widget.
   */
  protected function getMediaReferenceForm(array $reference_form, FormStateInterface $form_state) {
    $labels = $reference_form['#ief_labels'];
    $ief_id = $reference_form['#ief_id'];
    $target_bundle = $this->getSetting('target_bundle');

    $selection_settings = [
      'match_operator' => $this->getSetting('match_operator'),
      'target_bundles' => ["$target_bundle" => $target_bundle],
      'sort' => ['field' => '_none', 'direction' => 'ASC'],
      'auto_create' => FALSE,
      'auto_create_bundle' => '',
    ];

    $reference_form['#title'] = $this->t('Add existing @type_singular', ['@type_singular' => $labels['singular']]);

    $reference_form['entity_id'] = [
      '#type' => 'entity_autocomplete',
      // @todo Use bundle defined singular/plural labels as soon as
      //   https://www.drupal.org/node/2765065 is committed.
      // @see https://www.drupal.org/node/2765065
      '#title' => t('@label', ['@label' => ucfirst($labels['singular'])]),
      '#target_type' => 'media',
      '#selection_handler' => 'default:media',
      '#selection_settings' => $selection_settings,
      '#required' => TRUE,
      '#maxlength' => 255,
    ];
    // Add the actions
    $reference_form['actions'] = [
      '#type' => 'container',
      '#weight' => 100,
    ];
    $reference_form['actions']['ief_reference_save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add @type_singular', ['@type_singular' => $labels['singular']]),
      '#name' => 'ief-reference-submit-' . $reference_form['#ief_id'],
      '#limit_validation_errors' => [$reference_form['#parents']],
      '#attributes' => ['class' => ['ief-entity-submit']],
      '#ajax' => [
        'callback' => 'inline_entity_form_get_element',
        'wrapper' => 'inline-entity-form-' . $reference_form['#ief_id'],
      ],
    ];

    parent::addSubmitCallbacks($reference_form['actions']['ief_reference_save']);

    $reference_form['actions']['ief_reference_cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#name' => 'ief-reference-cancel-' . $reference_form['#ief_id'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => 'inline_entity_form_get_element',
        'wrapper' => 'inline-entity-form-' . $reference_form['#ief_id'],
      ],
      '#submit' => [['\Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex', 'closeForm']],
    ];

    $reference_form['#element_validate'][] = 'inline_entity_form_reference_form_validate';
    $reference_form['#ief_element_submit'][] = 'inline_entity_form_reference_form_submit';

    // Allow other modules to alter the form.
    $this->moduleHandler->alter('inline_entity_form_reference_form', $reference_form, $form_state);

    return $reference_form;
  }

}
